<?php
namespace Maagit\Maagitblog\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitblog
	Package:			Controller
	class:				BlogController

	description:		Main class for the blog.
						Process the actions "list" and "category".

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2022-10-09	Urs Maag		Remove objectManager in "initializeObject"
													return ResponseInterface in Actions

------------------------------------------------------------------------------------- */


class BlogController extends \Maagit\Maagitblog\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitblog\Domain\Repository\BlogRepository
     */
    protected $blogRepository;
	
	/**
	 * @var \Maagit\Maagitblog\Domain\Repository\PostRepository
	 */
	protected $postRepository;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// inject repositories
		$this->blogRepository = $this->makeInstance('Maagit\\Maagitblog\\Domain\\Repository\\BlogRepository');
		$this->postRepository = $this->makeInstance('Maagit\\Maagitblog\\Domain\\Repository\\PostRepository');
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Action for this controller.
	 * list blog entries.
     *
     * @return \Psr\Http\Message\ResponseInterface							the response
     */
    public function listAction()
    {
		$blogUid = (int)($this->settings['blog'] ?? 0);
		$blog = $this->blogRepository->findByUid($blogUid);
		$posts = $this->postRepository->findByBlog($blog);
		$this->validateAndRedirect($posts);
		$this->view->assign('blog', $blog);
		$this->view->assign('posts', $posts);
		$paginationService = $this->makeInstance('Maagit\\Maagitblog\\Service\\PaginationService', $posts, $this->getPageFromUrl(), $this->settings['paginate']['postsPerPage'], $this->settings['paginate']['maxLinks']);
		$this->view->assign('pagination', $paginationService->getPaginationArguments());
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }

	/**
     * Action for this controller.
	 * display a list of posts, filtered by category.
     *
     * @return \Psr\Http\Message\ResponseInterface							the response
     */
    public function categoryAction(\Maagit\Maagitblog\Domain\Model\Category $category)
    {
		$blogUid = (int)($this->settings['blog'] ?? 0);
		$blog = $this->blogRepository->findByUid($blogUid);
		$posts = $this->postRepository->findByCategory($blog, $category);
		$this->validateAndRedirect($posts);
		$this->view->assign('blog', $blog);
		$this->view->assign('posts', $posts);
		$this->view->assign('category', $category);
		$paginationService = $this->makeInstance('Maagit\\Maagitblog\\Service\\PaginationService', $posts, $this->getPageFromUrl(), $this->settings['paginate']['postsPerPage'], $this->settings['paginate']['maxLinks']);
		$this->view->assign('pagination', $paginationService->getPaginationArguments());
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * get page number from url
     *
     * @return int
     */
	private function getPageFromUrl()
	{
		// get from paginate parameter, if available
		if (!empty($this->request->getArguments()['currentPage']))
		{
			return (int)$this->request->getArguments()['currentPage'];
		}
		return 0;
	}
	
	/**
     * validate uri and redirect, if necessary
     *
     * @param	object			$posts			the current posts
	 * @return	void
     */
	protected function validateAndRedirect(object $posts)
	{
		// validate uri
		$uriService = $this->makeInstance('Maagit\\Maagitblog\\Service\\UriService');
		$uriService->checkPageAndRedirect($this, $posts);
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}