<?php
namespace Maagit\Maagitblog\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */

https://test.maagit.ch/blog?tx_maagitblog_pi1%5Baction%5D=accept&tx_maagitblog_pi1%5Bcontroller%5D=Comment&cHash=a9b995f6a91188aefafe9a1da6a39cd2


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitblog
	Package:			Controller
	class:				CommentController

	description:		Create and activate comments.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2022-10-09	Urs Maag		Typo3 12.0.0 compatibility
													- Remove objectManager in "initializeObject"
													- return ResponseInterface in Actions
													- initializie variable "$message"

------------------------------------------------------------------------------------- */


class CommentController extends \Maagit\Maagitblog\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitblog\Domain\Repository\PostRepository
	 */
	protected $postRepository;
	
	/**
	 * @var \Maagit\Maagitblog\Domain\Repository\CommentRepository
	 */
	protected $commentRepository;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// inject repositories
		$this->postRepository = $this->makeInstance('Maagit\\Maagitblog\\Domain\\Repository\\PostRepository');
		$this->commentRepository = $this->makeInstance('Maagit\\Maagitblog\\Domain\\Repository\\CommentRepository');
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Add a comment to a blog post and redirect to single view
     *
     * @param	\Maagit\Maagitblog\Domain\Model\Post		$post       	the post
     * @param	\Maagit\Maagitblog\Domain\Model\Comment 	$newComment 	the comment
     * @return	\Psr\Http\Message\ResponseInterface							the response
     */
    public function createAction(\Maagit\Maagitblog\Domain\Model\Post $post, \Maagit\Maagitblog\Domain\Model\Comment $newComment)
    {
		// check comment, if it's spam
		$spam = ($this->settings['spam']['enabled'] && ($this->hasLinks($newComment) || $this->inText($newComment, explode(',', $this->settings['spam']['words']))));

		// process comment
		if (!$spam)
		{
			// set comment to "hidden", if setting "check comments" is enabeld
			$message = '';
			if ($this->settings['checkComment'])
			{
				$newComment->setHidden(true);
				$message = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('comment.message', 'maagitblog');
			}
			// add new comment given post
			$post->addComment($newComment);
			$this->postRepository->update($post);

			// persist all, to get the created comment uid
			$persistenceManager = $this->makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');
			$persistenceManager->persistAll();

			// send mail, if setting "check comments" is enabled
			if ($this->settings['checkComment'] || $this->settings['informComment'])
			{
				$emailService = $this->makeInstance('Maagit\\Maagitblog\\Service\\EmailService');
				$renderService = $this->makeInstance('Maagit\\Maagitblog\\Service\\RenderService');
				$body = $renderService->renderTemplate('Email/NewComment', $this->getEmailVariables($post, $newComment));
				$emailService->sendMail(
					array($newComment->getEmail() => $newComment->getAuthor()), 
					array($this->settings['email'] => $this->settings['name']), 
					\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mail.newComment.subject', 'maagitblog'), 
					$body
				);
			}
		}
		else
		{
			$message = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('comment.message.spam', 'maagitblog');
		}

		// redirect to given post
		return $this->redirect('show', 'Post', null, $post->getLinkParameter($message));
    }
	
	/**
     * Update a accepted comment to "hidden=0"
     *
     * @param	int 	 										 $commentUid      		The comment's uid
     * @return	\Psr\Http\Message\ResponseInterface										the response
     */
    public function acceptAction(int $commentUid)
    {
		$comment = $this->commentRepository->findHiddenEntryByUid($commentUid);
		$comment->setHidden(false);
		$this->commentRepository->update($comment);
		$this->view->assign('post', $comment->getPost());
		$this->view->assign('comment', $comment);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Validation settings for the new comment
     *
     * @param	-
     * @return	void
     */
	protected function initializeCreateAction()
	{
		$propertyMappingConfiguration = $this->arguments['newComment']->getPropertyMappingConfiguration();
		$propertyMappingConfiguration->allowAllProperties();
		$propertyMappingConfiguration->setTypeConverterOption('TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE);
	}

	/**
     * Creates a array of variables to use in email fluid template
     *
     * @param	\Maagit\Maagitblog\Domain\Model\Post		$post			the post
     * @param	\Maagit\Maagitblog\Domain\Model\Comment		$comment		the comment
     * @return	array
     */
	protected function getEmailVariables(\Maagit\Maagitblog\Domain\Model\Post $post, \Maagit\Maagitblog\Domain\Model\Comment $comment): array
	{
		$variables = array();
		$variables['blog']['title'] = $post->getBlog()->getTitle();
		$variables['post']['title'] = $post->getTitle();
		$variables['post']['comment']['text'] = $comment->getComment();
		$variables['post']['comment']['author'] = $comment->getAuthor();
		$variables['post']['comment']['email'] = $comment->getEmail();
		$variables['post']['comment']['uid'] = $comment->getUid();
		$variables['settings']['checkComment'] = $this->settings['checkComment'];
		$variables['settings']['informComment'] = $this->settings['informComment'];
		return $variables;
	}

	/**
     * Check, if there are links in given comment
     *
     * @param	\Maagit\Maagitblog\Domain\Model\Comment		$comment		the comment
     * @return	boolean
     */
	protected function hasLinks(\Maagit\Maagitblog\Domain\Model\Comment $comment)
	{
		if ($this->settings['spam']['nolinks'])
		{
			$pattern = '~[a-z]+://\S+~';
			$num_found = preg_match_all($pattern, $comment->getComment(), $out);
			if ($num_found > 0)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
     * Check, if there are given words in the comment
     *
     * @param	\Maagit\Maagitblog\Domain\Model\Comment		$comment		the comment
	 * @param	array										$words			the words
     * @return	boolean
     */
	protected function inText(\Maagit\Maagitblog\Domain\Model\Comment $comment, array $words)
	{
		$found = false;
		foreach ($words as $word)
		{
			if (!empty(trim($word)))
			{
				if (strpos(strtolower($comment->getComment()), strtolower(trim($word))) !== FALSE)
				{
					$found = true;
					break;
				}
			}
		}
		return $found;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}