<?php
namespace Maagit\Maagitblog\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitblog
	Package:			Controller
	class:				PostController

	description:		Show a blog post.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2022-10-09	Urs Maag		Remove objectManager in "initializeObject"
													return ResponseInterface in Actions

------------------------------------------------------------------------------------- */


class PostController extends \Maagit\Maagitblog\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitblog\Domain\Repository\CategoryRepository
     */
    protected $categoryRepository;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Contructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// inject repositories
		$this->categoryRepository = $this->makeInstance('Maagit\\Maagitblog\\Domain\\Repository\\CategoryRepository');
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Displays a given post.
     *
     * @param	\Maagit\Maagitblog\Domain\Model\Post		$post			the post
	 * @param	\Maagit\Maagitblog\Domain\Model\Comment		$newComment		a new comment
	 * @param	string  									$message    	mesasge, after submit of a new comment
	 * @return	\Psr\Http\Message\ResponseInterface							the response
     */
    public function showAction(\Maagit\Maagitblog\Domain\Model\Post $post, \Maagit\Maagitblog\Domain\Model\Comment $newComment=null, string $message=null)
    {
		if ($newComment === null)
		{
            $newComment = $this->makeInstance('Maagit\\Maagitblog\\Domain\\Model\\Comment');
        }
		$this->view->assign('post', $post);
		$this->view->assign('newComment', $newComment);
		$this->view->assign('message', $message);
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}