<?php
namespace Maagit\Maagitblog\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitblog
	Package:			Repository
	class:				PostRepository

	description:		Repository for the "post" model.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2022-10-09	Urs Maag		Remove objectManager
													change "findByCategory" to query
	 												categories with "in" operator

------------------------------------------------------------------------------------- */


class PostRepository extends \Maagit\Maagitblog\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var array
     */
	protected $defaultOrderings = [
		'date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
	];


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    /**
     * Initialize the object, set default values
     *
     */
	public function initializeObject()
	{
		parent::initializeObject();
		$querySettings = $this->makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($querySettings);
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * find all objects with matching category
     *
     * @param	\Maagit\Maagitblog\Domain\Model\Blog							$blog			the blog
	 * @param	\Maagit\Maagitblog\Domain\Model\Category						$category		the category
     * @return	array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface					the record
     */
	public function findByCategory(\Maagit\Maagitblog\Domain\Model\Blog $blog, \Maagit\Maagitblog\Domain\Model\Category $category)
	{
		// create query object
		$query = $this->createQuery();
		
        // create where clause for category and its subcategories
		$cats = [];
        $cats[] = $category;
		$categoryRepository = $this->makeInstance('Maagit\\Maagitblog\\Domain\\Repository\\CategoryRepository');
		$categories = $categoryRepository->getChildCategories($category);
        if (!is_null($categories) && count($categories) > 0)
		{
            foreach ($categories as $childCategory)
			{
                $cats[] = $childCategory;
            }
        }
		
		// add where clause
		$query->matching(
			$query->logicalAnd(
					$query->equals('blog', $blog),
					$query->logicalOr($query->contains('categories', $cats))
			)
		);

		// execute query and return result
		return $query->execute();
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}