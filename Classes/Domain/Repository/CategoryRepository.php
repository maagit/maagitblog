<?php
namespace Maagit\Maagitblog\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitblog
	Package:			Repository
	class:				CategoryRepository

	description:		Repository for the "category" model.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-12-24	Urs Maag		PHP 8, define all properties
													($childCategories)

------------------------------------------------------------------------------------- */


class CategoryRepository extends \Maagit\Maagitblog\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var array
     */
	protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
        'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
    ];
	
	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
	protected $childCategories;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    /**
     * Initialize the object, set default values
     *
     */
	public function initializeObject()
	{
		parent::initializeObject();
		$querySettings = $this->makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(false);
		$this->setDefaultQuerySettings($querySettings);
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * Returns all children of the given category
     *
     * @param	\Maagit\Maagitblog\Domain\Model\Category					$category		the category
     * @return	array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface					the result
     */
    public function findChildren($category)
    {
        if (!$category->isFirstLevel())
		{
            return null;
        }
        $query = $this->createQuery();
        $query->matching(
            $query->equals('parent', $category->getUid())
        );
        return $query->execute();
    }
	
    /**
     * get all child categories.
     *
	 * @param	\Maagit\Maagitblog\Domain\Model\Category		$category		the category
     * @return	null|ObjectStorage												the child categories
     */
	public function getChildCategories(\Maagit\Maagitblog\Domain\Model\Category $category)
	{
		if (!$category->isFirstLevel()) {
			return null;
		}
		if ($category->getChildCategories() == null) {
            $category->setChildCategories($this->makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage'));
			$categories = $this->findChildren($category);
            foreach ($categories as $cat) {
                $childs = $category->getChildCategories();
				$childs->attach($cat);
				$category->setChildCategories($childs);
            }
        }
        return $this->childCategories;
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
