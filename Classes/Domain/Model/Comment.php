<?php
namespace Maagit\Maagitblog\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitblog
	Package:			Model
	class:				Category

	description:		Model for the "comment".
						Inherits datas of the comments.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2022-10-09	Urs Maag		Typo3 12.0.0 compatibility
													- Add property "getHidden"
													- Init member "$hidden" with boolean value

------------------------------------------------------------------------------------- */


class Comment extends \Maagit\Maagitblog\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var \Maagit\Maagitblog\Domain\Model\Post
     */
    protected $post;
	
	/**
     * @var string
     */
    protected $date;

    /**
     * @var string
     */
    protected $author = '';

    /**
     * @var string
     */
    protected $email = '';

    /**
     * @var string
     */
    protected $comment = '';

    /**
     * @var bool
     */
    protected $hidden = false;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
    /**
     * Sets the blog this post is part of
     *
     * @param \Maagit\Maagitblog\Domain\Model\Blog	$blog
     */
    public function setPost(\Maagit\Maagitblog\Domain\Model\Post $post)
    {
        $this->post = $post;
    }

    /**
     * Returns the post this comment is part of
     *
     * @return \Maagit\Maagitblog\Domain\Model\Post	$post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Setter for date
     *
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date)
    {
		$this->date = $date->format('Y-m-d H:i:s');
    }

    /**
     * Getter for date
     *
     * @return \DateTime $date
     */
    public function getDate()
    {
        return new \DateTime($this->date);
    }

    /**
     * Sets the author for this comment
     *
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * Getter for author
     *
     * @return string	$author
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Sets the authors email for this comment
     *
     * @param string	$email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Getter for authors email
     *
     * @return string	$email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the content for this comment
     *
     * @param string	$comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * Getter for comment
     *
     * @return string	$comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Getter for hidden
     *
     * @return boolean	$hidden
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Sets the hide flag for this comment
     *
     * @param boolean	$hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
