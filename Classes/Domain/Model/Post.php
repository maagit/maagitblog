<?php
namespace Maagit\Maagitblog\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitblog
	Package:			Model
	class:				Post

	description:		Model for the "post".
						Inherits datas of the post.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2022-10-09	Urs Maag		Change BackendUser class

------------------------------------------------------------------------------------- */


class Post extends \Maagit\Maagitblog\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var \Maagit\Maagitblog\Domain\Model\Blog
     */
    protected $blog;

    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var \TYPO3\CMS\Beuser\Domain\Model\BackendUser
     */
    protected $author;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitblog\Domain\Model\Content>
     */
    protected $preview;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitblog\Domain\Model\Content>
     */
    protected $posttext;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category>
     */
    protected $categories;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitblog\Domain\Model\Comment>
     */
    protected $comments;
	
    /**
     * @var \Maagit\Maagitblog\Domain\Repository\CommentRepository
     */
    protected $commentRepository = null;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    /**
     * Constructs this post
     */
    public function initializeObject()
    {
        $this->categories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->comments = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->preview = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->posttext = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->date = new \DateTime();
    }


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
    /**
     * Sets the blog this post is part of
     *
     * @param \Maagit\Maagitblog\Domain\Model\Blog	$blog
     */
    public function setBlog(\Maagit\Maagitblog\Domain\Model\Blog $blog)
    {
        $this->blog = $blog;
    }

    /**
     * Returns the blog this post is part of
     *
     * @return \Maagit\Maagitblog\Domain\Model\Blog	$blog
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * Setter for title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Getter for title
     *
     * @return string	$title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Setter for date
     *
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * Getter for date
     *
     *
     * @return \DateTime	$date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets the author for this post
     *
     * @param \TYPO3\CMS\Beuser\Domain\Model\BackendUser $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * Getter for author
     *
     * @return \TYPO3\CMS\Beuser\Domain\Model\BackendUser	$author
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Getter for preview
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage	$preview
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * Getter for posttext
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage	$posttext
     */
    public function getPosttext()
    {
        return $this->posttext;
    }

    /**
     * Adds a Comment
     *
     * @param \Maagit\Maagitblog\Domain\Model\Comment	$comment
     */
    public function addComment(\Maagit\Maagitblog\Domain\Model\Comment $comment)
    {
        $this->comments->attach($comment);
    }

    /**
     * Returns the comments to this post
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage	$comments
     */
    public function getComments()
    {
        return $this->comments;
    }

	/**
	 * Set categories
	 *
	 * @param  \TYPO3\CMS\Extbase\Persistence\ObjectStorage	$categories
	 */
	public function setCategories($categories)
	{
	    $this->categories = $categories;
	}

    /**
     * Returns the categories
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category>	$categories
     */
    public function getCategories()
    {
        return $this->categories;
    }
	
    /**
     * get link parameters.
     *
     * @param	string			$message			the message to show (after saving comment)
	 * @return	array								the parameter array
     */
    public function getLinkParameter(string $message=null)
    {
		$page = (isset($_GET['tx_maagitblog_pi1']['currentPage'])) ? $_GET['tx_maagitblog_pi1']['currentPage'] : 1;	
		$params = array();
		$params['post'] = $this->getUid();
		if ($message != null) {$params['message'] = $message;}
		$uriService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitblog\Service\UriService::class);
		$params = array_merge($params, $uriService->getUrlArguments());
		return $params;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}