<?php
namespace Maagit\Maagitblog\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitblog
	Package:			Model
	class:				Category

	description:		Model for the "category".
						Inherits datas of the category.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Category extends \Maagit\Maagitblog\Domain\Model\BaseModelCategory
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitblog\Domain\Model\Category>
     */
    protected $childCategories;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
    /**
     * Set $childCategories
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitblog\Domain\Model\Category>	$childCategories
     */
    public function setChildCategories(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $childCategories)
    {
        $this->childCategories = $childCategories;
    }
	
    /**
     * Get $childCategories
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Maagit\Maagitblog\Domain\Model\Category>	$childCategories
     */
    public function getChildCategories()
    {
        return $this->childCategories;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * get "is this the first level?"
     *
     * @return bool	$isFirstLevel
     */
    public function isFirstLevel()
    {
        if ($this->parent) {
            return false;
        }
        return true;
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
