<?php
namespace Maagit\Maagitblog\Service;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitevent
	Package:			Service
	class:				PaginationService

	description:		Methods for paginating objects.

	created:			2020-12-26
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-12-26	Urs Maag		Initial version
						2024-09-19	Urs Maag		Method "buildPagination", set type
													of page number as string

------------------------------------------------------------------------------------- */


class PaginationService extends \Maagit\Maagitblog\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     * @var TYPO3\CMS\Extbase\Pagination\QueryResultPaginator|TYPO3\CMS\Core\Pagination\ArrayPaginator
     */
	protected $paginator;

	/**
     * @var int
     */
	protected $currentPage = 1;

	/**
     * @var int
     */
	protected $itemsPerPage = 50;

	/**
     * @var int
     */
	protected $maximumNumberOfLinks = 99;

	/**
     * @var int
     */
	protected $displayRangeStart;

    /**
     * @var int
     */
	protected $displayRangeEnd;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Initialize the object
     *
     */
	public function initializeObject($objects, int $currentPage=0, int $itemsPerPage=0, int $maxLinks=0)
	{
		// initialize member variables
		$this->currentPage = ($currentPage > 0) ? $currentPage : $this->currentPage;
		$this->itemsPerPage = ($itemsPerPage > 0) ? $itemsPerPage : $this->itemsPerPage;
		$this->maximumNumberOfLinks = ($maxLinks > 0) ? $maxLinks : $this->maximumNumberOfLinks;

		// get paginator object
		if ($objects instanceof \TYPO3\CMS\Extbase\Persistence\QueryResultInterface)
		{
			$this->paginator = $this->makeInstance('TYPO3\\CMS\\Extbase\\Pagination\\QueryResultPaginator', $objects, $this->currentPage, $this->itemsPerPage);
		}
		else
		{
			$this->paginator = $this->makeInstance('TYPO3\\CMS\\Core\\Pagination\\ArrayPaginator', $objects, $this->currentPage, $this->itemsPerPage);
		}
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * create pagination arguments for using in a view
	 *
	 * @return	array									the paginate arguments
     */
	public function getPaginationArguments()
	{
		$paginationArguments = $this->buildPagination();
		$paginationArguments['paginatedItems'] = $this->paginator->getPaginatedItems();
		return $paginationArguments;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * build pagination arguments, based on given member variables
     *
     * @return array
     */
    protected function buildPagination()
    {
		$this->calculateDisplayRange();
		$pages = array();
		for ($i = $this->displayRangeStart; $i <= $this->displayRangeEnd; $i++)
		{
			$pages[] = array('number' => (string)$i, 'isCurrent' => ($i === $this->currentPage));
		}
		$pagination = array(
			'pages' => $pages,
			'current' => $this->currentPage,
			'numberOfPages' => $this->paginator->getNumberOfPages(),
			'displayRangeStart' => $this->displayRangeStart,
			'displayRangeEnd' => $this->displayRangeEnd,
			'hasLessPages' => $this->displayRangeStart > 2,
			'hasMorePages' => $this->displayRangeEnd + 1 < $this->paginator->getNumberOfPages()
		);
		if ($this->currentPage < $this->paginator->getNumberOfPages())
		{
//			$pagination['nextPage'] = $this->currentPage + 1;
		}
		if ($this->currentPage > 1) {
//            $pagination['previousPage'] = $this->currentPage - 1;
		}
		return $pagination;
    }

    /**
     * calculate display range, based on given member variables
     *
     * @return void
     */
    protected function calculateDisplayRange()
    {
		$maximumNumberOfLinks = $this->maximumNumberOfLinks;
		if ($maximumNumberOfLinks > $this->paginator->getNumberOfPages())
		{
			$maximumNumberOfLinks = $this->paginator->getNumberOfPages();
		}
		$delta = floor($maximumNumberOfLinks / 2);
		$this->displayRangeStart = $this->currentPage - $delta;
		$this->displayRangeEnd = $this->currentPage + $delta - ($maximumNumberOfLinks % 2 === 0 ? 1 : 0);
		if ($this->displayRangeStart < 1)
		{
			$this->displayRangeEnd -= $this->displayRangeStart - 1;
		}
		if ($this->displayRangeEnd > $this->paginator->getNumberOfPages())
		{
			$this->displayRangeStart -= $this->displayRangeEnd - $this->paginator->getNumberOfPages();
		}
		$this->displayRangeStart = (int)max($this->displayRangeStart, 1);
		$this->displayRangeEnd = (int)min($this->displayRangeEnd, $this->paginator->getNumberOfPages());
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}