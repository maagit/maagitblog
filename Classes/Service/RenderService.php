<?php
namespace Maagit\Maagitblog\Service;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitblog
	Package:			Service
	class:				RenderService

	description:		Various methods, used for rendering with fluid.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2022-10-09	Urs Maag		Typo3 12.0.0 compatibility
													- create renderingContext in
													  method "renderTemplate"

------------------------------------------------------------------------------------- */


class RenderService extends \Maagit\Maagitblog\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * render a template
     *
	 * @param string 										$template		The fluid template
	 * @param array 										$assigns		Key/Value pairs of data's to assign
     * @return string
     */
	 public function renderTemplate(string $template, array $assigns=array())
	 {
		 $view = $this->makeInstance('TYPO3\\CMS\\Fluid\\View\\StandaloneView', $this->divHelper->getRenderingContext());
		 $template .= (end(explode('.', $template))) ? '' : '.html';
		 $view->setTemplate($template);
		 $view->setTemplateRootPaths($this->settings['view']['templateRootPaths']);
		 $view->setPartialRootPaths($this->settings['view']['partialRootPaths']);
		 foreach ($assigns as $key => $data)
		 {
			 $view->assign($key, $data);
		 }
		 return $view->render();
	 }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
