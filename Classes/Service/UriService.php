<?php
namespace Maagit\Maagitblog\Service;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitblog
	Package:			Service
	class:				UriService

	description:		Various methods, used for creating urls.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class UriService extends \Maagit\Maagitblog\Service\BaseService 
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var array
     */
    protected $excludeArguments = array('post', 'action', 'controller', 'message');


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * get url arguments
     *
	 * @param -
     * @return array								relevant url arguments for a blog uri
     */
	public function getUrlArguments()
	{
		if (!isset($_GET ['tx_maagitblog_pi1'])) {return array();}
		$urlParams = $_GET ['tx_maagitblog_pi1'];
		$args = array();
		foreach ($urlParams as $key => $value)
		{
			if (!in_array($key, $this->excludeArguments))
			{
				$args[$key] = $value;
			}
		}
		return $args;
	}

	/**
     * Validate given uri and - if there are changes - redirect
     *
	 * @param	\Maagit\Maagitblog\Controller\BaseController			$uri		the uri to check
	 * @param	object													$posts		the current posts
	 * @return	void
     */
	public function checkPageAndRedirect(\Maagit\Maagitblog\Controller\BaseController $controller, object $posts)
	{
		if (isset($_GET['tx_maagitblog_pi1']['currentPage']) && isset($this->settings['paginate']['postsPerPage']))
		{
			$currentPage = $_GET['tx_maagitblog_pi1']['currentPage'];
			$postsPerPage = $this->settings['paginate']['postsPerPage'];
			$pagesOnCurrentSelection = ceil(count($posts) / $postsPerPage);
			if ($pagesOnCurrentSelection < $currentPage)
			{
				$request = $GLOBALS['TYPO3_REQUEST'];
				$normalizedParams = $request->getAttribute('normalizedParams');
				$uri = $normalizedParams->getRequestUrl();
				$uri = str_replace(
					'tx_maagitblog_pi1%5BcurrentPage%5D='.$currentPage,
					'tx_maagitblog_pi1%5BcurrentPage%5D='.$pagesOnCurrentSelection,
					$uri
				);
				$uri = substr($uri, 0, strrpos($uri, '&cHash='));
				$this->divHelper->callByReflection($controller, 'redirectToURI', array($uri, $delay = 0, $statusCode = 303));
			}
		}
	}	


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
