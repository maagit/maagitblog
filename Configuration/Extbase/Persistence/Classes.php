<?php
declare(strict_types = 1);

return [
    // Content property mapping
	\Maagit\Maagitblog\Domain\Model\Content::class => [
        'tableName' => 'tt_content',
    ],

	// Category property mapping
	\Maagit\Maagitblog\Domain\Model\Category::class => [
        'tableName' => 'sys_category',
        'recordType' => \Maagit\Maagitblog\Domain\Model\Category::class,
    ]

];