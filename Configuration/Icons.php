<?php
	return [
		'extensions-maagitblog_pi1' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
			'source' => 'EXT:maagitblog/Resources/Public/Icons/module-blog.svg',
		]
	];
?>