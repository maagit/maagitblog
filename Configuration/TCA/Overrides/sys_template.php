<?php
call_user_func(function () {
	// add as static template (used for correct extbase table mapping)
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('maagitblog', 'Configuration/TypoScript', 'Maagitblog');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('maagitblog', 'Configuration/TypoScript/DefaultStyles', 'Maagitblog CSS Styles (optional)');
});
?>