<?php
defined('TYPO3') || die('Access denied.');

// Add type icon class
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['maagitblog_pi1'] = 'extensions-maagitblog_pi1';

// make post records categorizeable
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_maagitblog_domain_model_post', 'categories');

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagitblog',																			// extension name
    'Pi1',																					// plugin name
    'LLL:EXT:maagitblog/Resources/Private/Language/locallang.xlf:plugins.title',			// plugin title
    'extensions-maagitblog_pi1',															// icon identifier
    'maagitblog', 	 																		// group
    'LLL:EXT:maagitblog/Resources/Private/Language/locallang.xlf:plugins.description'		// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagitblog/Configuration/FlexForms/Frontend.xml',
	$pluginSignature
);
?>