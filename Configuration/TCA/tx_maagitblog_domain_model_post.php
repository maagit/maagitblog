<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_post',
        'label' => 'title',
        'label_alt' => 'author',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'delete' => 'deleted',
        'sortby' => 'sorting',
        'enablecolumns' => [
            'disabled' => 'hidden'
        ],
        'iconfile' => 'EXT:maagitblog/Resources/Public/Icons/icon_tx_maagitblog_domain_model_post.gif'
    ],
    'interface' => [
        'maxDBListItems' => 100,
        'maxSingleDBListItems' => 500
    ],
    'types' => [
        '1' => [
			'showitem' => '
				--div--;LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_post.tabs.post,
					blog, title, date, author, preview, posttext, sys_language_uid,
				--div--;LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_post.tabs.comment,
					comments,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access,
				--div--;LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_post.tabs.category,
					categories'
        ]
    ],
	'palettes' => [
		'visibility' => [
            'showitem' => 'hidden',
		],
        'access' => [
            'showitem' => '
				starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.starttime_formlabel,
				endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.endtime_formlabel,
				--linebreak--, fe_group;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.fe_group_formlabel',
        ]
	],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'language',
                'renderType' => 'selectSingle',
                'default' => 0
            ]
        ],
        'l18n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
                'default' => ''
            ]
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check'
            ]
        ],
        'categories' => [
			'config' => [
                'type' => 'category'
            ]
		],
		'blog' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_post.blog',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_maagitblog_domain_model_blog',
                'foreign_table_where' => 'AND tx_maagitblog_domain_model_blog.hidden = 0',
				'maxitems' => 1,
				'required' => true
            ]
        ],
        'title' => [
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_post.title',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 256,
				'required' => true
            ]
        ],
        'date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_post.date',
            'config' => [
                'type' => 'datetime',
                'size' => 12,
                'default' => time(),
				'required' => true
            ]
        ],
        'author' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_post.author',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'be_users',
                'foreign_table_where' => ' AND be_users.disable = 0 '.
                    'AND (be_users.username != "_cli_lowlevel" AND be_users.username != "_cli_scheduler") '.
                    'ORDER BY be_users.username',
                'minitems' => 1,
                'maxitems' => 1,
                'prepend_tname' => false,
			]
        ],
        'preview' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_post.preview',
            'config' => array(
                'type' => 'inline',
                'allowed' => 'tt_content',
                'foreign_table' => 'tt_content',
                'foreign_table_field' => 'irre_parenttable_preview',
                'foreign_field' => 'irre_parentid_preview',
                'minitems' => 1,
                'maxitems' => 1,
                'appearance' => array(
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                    'levelLinksPosition' => 'bottom',
                    'useSortable' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1,
                    'showSynchronizationLink' => 1,
                    'enabledControls' => array(
                        'info' => false,
                    ),
                ),
                'behaviour' => array(
                    'enableCascadingDelete' => true,
	            	'allowLanguageSynchronization' => true,
                ),
                'foreign_selector_fieldTcaOverride' => array(
                    'l10n_mode' => 'prefixLangTitle',
                ),
            )
        ],
		'posttext' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_post.posttext',
            'config' => array(
                'type' => 'inline',
                'allowed' => 'tt_content',
                'foreign_table' => 'tt_content',
                'foreign_table_field' => 'irre_parenttable_posttext',
                'foreign_field' => 'irre_parentid_posttext',
                'minitems' => 1,
                'maxitems' => 99,
                'appearance' => array(
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                    'levelLinksPosition' => 'bottom',
                    'useSortable' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1,
                    'showSynchronizationLink' => 1,
                    'enabledControls' => array(
                        'info' => false,
                    ),
                ),
                'behaviour' => array(
                    'enableCascadingDelete' => true,
	            	'allowLanguageSynchronization' => true,
                ),
                'foreign_selector_fieldTcaOverride' => array(
                    'l10n_mode' => 'prefixLangTitle',
                ),
            )
        ],
        'comments' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_post.comments',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_maagitblog_domain_model_comment',
				'foreign_table_where' => 'AND tx_maagitblog_domain_model_comment.hidden = 0',
                'foreign_field' => 'post',
                'size' => 10,
                'autoSizeMax' => 30,
                'multiple' => 0,
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                ]
            ]
        ]
    ]
];
