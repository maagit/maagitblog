<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xlf:tx_maagitblog_domain_model_blog',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden'
        ],
        'iconfile' => 'EXT:maagitblog/Resources/Public/Icons/icon_tx_maagitblog_domain_model_blog.gif'
    ],
    'interface' => [
        
    ],
    'types' => [
        '1' => [
			'showitem' => '
				--div--;LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xlf:tx_maagitblog_domain_model_blog.tabs.blog,
					title, description, sys_language_uid,
				--div--;LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xlf:tx_maagitblog_domain_model_blog.tabs.posts,
					posts,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.access,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.visibility;visibility,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.access;access'
        ]
    ],
	'palettes' => [
		'visibility' => [
            'showitem' => 'hidden',
		],
        'access' => [
            'showitem' => '
				starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.starttime_formlabel,
				endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.endtime_formlabel,
				--linebreak--, fe_group;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.fe_group_formlabel',
        ]
	],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'language',
                'default' => 0
            ]
        ],
        'l18n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
                'default' => ''
            ]
        ],
        't3ver_label' => [
            'displayCond' => 'FIELD:t3ver_label:REQ:true',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'none',
                'size' => 27
            ]
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check'
            ]
        ],
        'title' => [
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xlf:tx_maagitblog_domain_model_blog.title',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 256,
				'required' => true
            ]
        ],
        'subtitle' => [
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xlf:tx_maagitblog_domain_model_blog.subtitle',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 256
            ]
        ],
        'description' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xlf:tx_maagitblog_domain_model_blog.description',
            'config' => [
                'type' => 'text',
                'required' => true,
                'rows' => 30,
                'cols' => 80,
            ]
        ],
        'posts' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xlf:tx_maagitblog_domain_model_blog.posts',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_maagitblog_domain_model_post',
				'foreign_table_where' => 'AND tx_maagitblog_domain_model_post.hidden = 0',
                'foreign_field' => 'blog',
				'foreign_sortby' => 'sorting',
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                ]
            ]
        ]
	]
];
