<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_comment',
        'label' => 'date',
        'label_userFunc' => 'Maagit\\Maagitblog\\Userfuncs\\Tca->dateTitle',
        'label_alt' => 'author',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'delete' => 'deleted',
        'hideTable' => 1,
		'enablecolumns' => [
            'disabled' => 'hidden'
        ],
        'iconfile' => 'EXT:maagitblog/Resources/Public/Icons/icon_tx_maagitblog_domain_model_comment.gif'
    ],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check'
            ]
        ],
        'date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_comment.date',
            'config' => [
                'type' => 'datetime',
                'size' => 12,
				'required' => true
            ]
        ],
        'author' => [
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_comment.author',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 256,
				'required' => true
            ]
        ],
        'email' => [
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_comment.email',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 256,
				'required' => true
            ]
        ],
        'comment' => [
            'exclude' => true,
            'label' => 'LLL:EXT:maagitblog/Resources/Private/Language/locallang_db.xml:tx_maagitblog_domain_model_comment.comment',
            'config' => [
                'type' => 'text',
                'rows' => 30,
                'cols' => 80
            ]
        ],
        'post' => [
            'config' => [
                'type' => 'passthrough',
            ]
		]
    ],
    'types' => [
        '1' => ['showitem' => 'hidden, date, author, email, comment']
    ],
    'palettes' => [
        '1' => ['showitem' => '']
    ]
];
