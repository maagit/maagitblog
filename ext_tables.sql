#
# tx_maagitblog_domain_model_blog
#
CREATE TABLE tx_maagitblog_domain_model_blog (
	title varchar(255) DEFAULT '' NOT NULL,
	subtitle varchar(255) DEFAULT '',
	description text NOT NULL,
	posts varchar(255) DEFAULT '' NOT NULL
);

#
# tx_maagitblog_domain_model_post
#
CREATE TABLE tx_maagitblog_domain_model_post (
	blog int(11) DEFAULT '0' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	date int(11) DEFAULT '0' NOT NULL,
	author int(11) DEFAULT '0' NOT NULL,
	preview int(11) unsigned DEFAULT '0' NOT NULL,
	posttext int(11) unsigned DEFAULT '0' NOT NULL,
	comments int(11) unsigned DEFAULT '0' NOT NULL
);

#
# tt_content for post preview
#
CREATE TABLE tt_content (
	irre_parentid_preview int(11) DEFAULT '0' NOT NULL,
	irre_parenttable_preview tinytext DEFAULT '' NOT NULL,
	KEY maagitblog_preview (irre_parentid_preview,sorting)
);

#
# tt_content for post content
#
CREATE TABLE tt_content (
	irre_parentid_posttext int(11) DEFAULT '0' NOT NULL,
	irre_parenttable_posttext tinytext DEFAULT '' NOT NULL,
	KEY maagitblog_posttext (irre_parentid_posttext,sorting)
);

#
# tx_maagitblog_domain_model_comment
#
CREATE TABLE tx_maagitblog_domain_model_comment (
	post int(11) DEFAULT '0' NOT NULL,
	date datetime DEFAULT CURRENT_TIMESTAMP,
	author varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	comment text NOT NULL
);