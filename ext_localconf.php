<?php
defined('TYPO3') || die('Access denied.');

// Load userfuncs when TYPO3 is not in composer mode
if (!defined('TYPO3_COMPOSER_MODE') || !TYPO3_COMPOSER_MODE) {
	require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('maagitblog').'Classes/Userfuncs/Tca.php';
}

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagitblog',
	'Pi1',
	[
		\Maagit\Maagitblog\Controller\BlogController::class => 'list,category',
		\Maagit\Maagitblog\Controller\PostController::class => 'show',
		\Maagit\Maagitblog\Controller\CommentController::class => 'create,accept'
	],
	[
        \Maagit\Maagitblog\Controller\BlogController::class => 'list,category',
		\Maagit\Maagitblog\Controller\PostController::class => 'show',
		\Maagit\Maagitblog\Controller\CommentController::class => 'create,accept'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
